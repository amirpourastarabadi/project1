<?php

use Illuminate\Support\Facades\Route;
use App\Models\Post;


Route::get('/', function () {
    $posts = Post::with(['tags', 'categories', 'image', 'author'])->orderBy('id', 'desc')->get();
    return view('welcome', compact('posts'));
});


Route::get('/home', 'HomeController@index')->name('home');
