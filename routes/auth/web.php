<?php

use Illuminate\Support\Facades\Route;


Route::get('login', "AuthController@loginForm")->name('loginForm');
Route::post('checkMobilNumber', "AuthController@checkMobil")->name('checkMobile');
Route::post('login', "AuthController@login")->name('login');
Route::post('logout', "AuthController@logout")->name('logout');
Route::post('verificationMobil', "AuthController@verificationMobil")->name('verificationMobil');
