<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'slug'];
    public $timestamps = false;

    // Set tag slug base it's title
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = Str::random(7). "-" .Str::slug($value);
    }

    // Return all posts of a tag
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
