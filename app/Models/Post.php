<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['alt', 'image'];

    // Set slug base title
    public function setTitleAttribute($value = null)
    {

        if (!$value) {
            $this->attributes['title'] = request()->file('image')->getFilename();
        } else {
            $this->attributes['title'] = $value;
        }
        $this->attributes['slug'] = Str::random(7) . "-" . Str::slug($this->attributes['title']);
    }

    // Return all categories of a post
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    // Return all tags of a post
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    // Return image of a post
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    // Return author(instance of User class) of a post
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
