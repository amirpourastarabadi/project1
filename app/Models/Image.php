<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'alt', 'imageable_type', 'imageable_id', 'path'];
    public $timestamps = false;

//    public function setTitleAttribute($value = null){
//        dd('title');
//    }

    // Return related model to an image
    public function imageable()
    {
        return $this->morphTo();
    }


}
