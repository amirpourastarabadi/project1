<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    // Return all posts and tags, categories related to each post
    public function index()
    {
        $posts = auth()->user()->posts()->with(['tags', 'categories'])->latest()->get();
        return view('posts.index', compact('posts'));
    }

    // Return selected post from list
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    // Create new post
    public function create()
    {
        $tags = Tag::all();
        $categories = Category::all();
        return view('posts.create', compact('tags', 'categories'));
    }

    // Store Post
    public function store(PostRequest $request)
    {
        $request['path'] = "/storage/" . Storage::put('/posts/image', $request['image']);

        $post = auth()->user()->posts()->create($request->all());
//        dd($post);
//        $post = Post::create($request->all());

        $post->tags()->attach($request->tags);
        $post->categories()->attach($request->categories);
        $post->image()->create($request->all());

        return redirect()->route('posts.index')->with(['status' => __('posts.index.content.status')]);
    }

    // Delete a post
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('posts.index')->with(['status', __('posts.index.content.delete_alert')]);
    }
}
