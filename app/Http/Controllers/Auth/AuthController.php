<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\MobileRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{

    public function verificationMobil(Request $request)
    {
        if ($mobil = session($request['verification_code'])) {
            return view('auth.register')->with('mobil', $mobil);
        } else {
            return view('auth.mobilValidationForm')->with('message', __('auth.messages.invalidValidationCode'));
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('loginForm');
    }

    public function login(Request $request)
    {
        if (Hash::check($request['password'], session('user')->getAuthPassword())) {
            Auth::login(session('user'));
            return view('auth.dashboard');
        } else {
            return view('auth.passwordForm')->with('message', __('auth.invalid_password'));
        }
    }

    public function checkMobil(MobileRequest $request)
    {
        $user = User::where('mobil', $request['mobil'])->first();

        if ($user) {
            session()->put('user', $user);
            return view('auth.passwordForm');
        } else {
            session()->put(Str::random(7), $request['mobil']);
            return view('auth.mobilValidationForm');
        }
    }

    public function loginForm()
    {
        return view('auth.loginForm');
    }


}
