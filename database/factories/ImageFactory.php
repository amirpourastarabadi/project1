<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    protected $model = Image::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(rand(5, 10)),
            'alt' => $this->faker->sentence(rand(5, 10)),
            'path' => $this->faker->imageUrl(),
        ];
    }
}
