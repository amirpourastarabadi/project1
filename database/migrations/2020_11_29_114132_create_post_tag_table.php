<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostTagTable extends Migration
{
    private $table = "post_tag";

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id')
                ->references('id')
                ->on('posts')
                ->cascadeOnDelete();
            $table->foreignId('tag_id')
                ->references('id')
                ->on('tags')
                ->cascadeOnDelete();
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
