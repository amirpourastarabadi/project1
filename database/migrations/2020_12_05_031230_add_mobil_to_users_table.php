<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMobilToUsersTable extends Migration
{
    private $table = 'users';

    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->string('mobil', 15)->unique()->nullable()->after('email');
        });
    }


    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropIfExists();
        });
    }
}
