<div class="form-group text-right">
    <label>{{$label}}</label>
    <input
        type="{{$type}}"
        class="form-control"
        name="{{$name}}"
        value="{{old($name)}}">
    @error($name)
    <span class="text-danger">{{$message}}</span>
    @enderror
</div>
