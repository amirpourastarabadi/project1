<div class="form-group">
    <label for="{{$name}}">{{$title}}</label>
    <select class="form-control" id="{{$name}}" name="{{$name}}[]" multiple>
        @forelse($variables as $variable)
            <option value="{{$variable->id}}">{{$variable->title}}</option>
        @empty
        @endforelse
    </select>
</div>
