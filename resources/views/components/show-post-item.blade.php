
<div class="row my-3">
    <div class="col-{{$titleWidth ?? 3}}">
        {{__("posts.index.content.$title")}}:
    </div>
    <div class="col-{{$contentWidth ?? 8}} border">
        {{$post->$title}}
    </div>
</div>
