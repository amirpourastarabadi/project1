<div class="row my-3">
    <div class="col-3">
        {{$title}}
    </div>
    <div class="col-8 border">
        @forelse($item->$relation as $item)
            <span class="badge badge-sm badge-info">{{$item->title}}</span>
        @empty
            <span class="badge badge-sm badge-info">{{$noItem}}</span>
        @endforelse()

    </div>
</div>
