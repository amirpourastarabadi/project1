<div class="form-group text-right">
    <label>{{$label}}</label>
    <textarea class="form-control" name="{{$name}}" rows="{{$height??3}}" cols="{{$width??3}}">{{old($name)}}</textarea>
    @error($name)
    <span class="text-danger">{{$message}}</span>
    @enderror
</div>
