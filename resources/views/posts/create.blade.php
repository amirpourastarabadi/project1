@extends('layouts.main')

@section('page_title', __('posts.create.page_title'))


@section('content')
    <div class="container">
        {{--main content title--}}
        <div class="row my-4">
            <h4>
                {{__('posts.create.content.page_title')}}
            </h4>
        </div>
        {{--main content--}}
        <div class="row">
            <form action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
                @csrf

                @component('components.form-input', ['label'=>__('posts.create.content.form.form_title.title'),'type'=>'text','name'=>'title'])
                @endcomponent

                @component('components.form-text-area', ['label'=>__('posts.create.content.form.form_title.content'),'height'=>3,'width'=> 5,'name'=>'content'])
                @endcomponent

                @component('components.form-input', ['label'=>__('posts.create.content.form.form_title.image'),'type'=>'file','name'=>'image'])
                @endcomponent

                @component('components.form-input', ['label'=>__('posts.create.content.form.form_title.alt'),'type'=>'text','name'=>'alt'])
                @endcomponent

                @component('components.form-input', ['label'=>__('posts.create.content.form.form_title.alt'),'type'=>'hidden','name'=>'user_id'])
                @endcomponent

                @component('components.post-form-select', [
                            'title'=> __('posts.create.content.form.form_title.tags'),
                            'name'=> 'tags',
                            'variables'=> $tags,
                            ])
                @endcomponent

                @component('components.post-form-select', [
                            'title'=> __('posts.create.content.form.form_title.categories'),
                            'name'=> 'categories',
                            'variables'=> $categories,
                            ])
                @endcomponent


                <button type="submit" class="btn btn-primary w-100">{{__('posts.create.content.form.submit')}}</button>
            </form>
        </div>
    </div>
@endsection
