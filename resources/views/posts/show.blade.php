@extends('layouts.main')


@section('content')

    <div class="row mt-3 mb-5">
        <h5>{{__('posts.show.title')}}</h5>
    </div>
    <div class="row">
        <div class="col-8">
            @component('components.show-post-item', ['post'=>$post, 'title'=>'title'])
            @endcomponent

            @component('components.show-post-item', ['post'=>$post, 'title'=>'slug'])
            @endcomponent

            @component('components.show-post-item', ['post'=>$post, 'title'=>'content'])
            @endcomponent

            @component('components.show-form-select', [
                        'title' => __("posts.index.content.categories"),
                        'item' => $post,
                        'relation' => 'categories',
                        'noItem' => __('posts.show.no_categories'),
                        ])
            @endcomponent

            @component('components.show-form-select', [
                        'title' => __("posts.index.content.tags"),
                        'item' => $post,
                        'relation' => 'tags',
                        'noItem' => __('posts.show.no_tags'),
                        ])
            @endcomponent
        </div>
        <div class="col-4 my-3">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{$post->image->path ?? '/noImage.png'}}" width="100%" height="200" alt="Card image cap">
            </div>
        </div>
    </div>





@endsection
