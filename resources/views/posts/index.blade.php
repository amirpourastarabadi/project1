@extends('layouts.main')


@section('page_title', __('posts.index.page_title'))


@section('content')
    {{--    header--}}
    <div class="container">
        <div class="row my-4">
            <h4>{{__('posts.index.content.page_title')}}</h4>
        </div>
        @if(session('status'))
            <div class="row">
                <div class="col-12 alert alert-success text-center">{{session('status')}}</div>
            </div>
        @endif
        @auth()
            <div class="row mb-2">
                <a href="{{route('posts.create')}}" class="btn btn-success">
                    {{__('posts.index.content.create_post')}}
                </a>

            </div>
        @endauth

        {{--    main content--}}
        <div class="row">
            <table class="table">
                <thead>
                <tr class="d-flex text-center">
                    <th scope="col" class="col-1">{{__('posts.index.content.id')}}</th>
                    <th scope="col" class="col-2">{{__('posts.index.content.title')}}</th>
                    <th scope="col" class="col-2">{{__('posts.index.content.slug')}}</th>
                    <th scope="col" class="col-2">{{__('posts.index.content.author')}}</th>
                    <th scope="col" class="col-1">{{__('posts.index.content.categories')}}</th>
                    <th scope="col" class="col-1">{{__('posts.index.content.tags')}}</th>
                    @auth()
                        <th scope="col" class="col-3">{{__('posts.index.content.operations')}}</th>
                    @endauth
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr class="d-flex text-center">
                        <td class="col-1">{{$post->id}}</td>
                        <td class="col-2">
                            <a href="{{route('posts.show', $post)}}" class="text-dark">
                                {{$post->title}}
                            </a>
                        </td>
                        <td class="col-2">{{$post->slug}}</td>
                        <td class="col-2">{{$post->author->name}}</td>
                        <td class="col-1">
                            @foreach($post->categories as $category)
                                <span class="badge badge-dark">{{$category->title}}</span>
                            @endforeach
                        </td>
                        <td class="col-1">
                            @foreach($post->tags as $tag)
                                <span class="badge badge-dark">{{$tag->title}}</span>
                            @endforeach
                        </td>
                        @auth()
                            <td class="col-3">
                                <form action="{{route('posts.destroy', $post)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    @can('delete', $post)
                                        <button class="btn btn-danger btn-sm"
                                                onclick="return confirm('آیا در حذف این پست عزم راسخ دارید؟؟')">{{__('posts.index.content.delete')}}</button>
                                    @endcan
                                </form>

                            </td>
                        @endauth
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection
