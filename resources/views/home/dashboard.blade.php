@extends('layouts.main')

@section('content')
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('home.index.dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <h4 class="mt-3 mb-5">
                            <b>{{ auth()->user()->name }}</b>
                            {{__('home.index.welcome') }}
                        </h4>
                        <h5>
                            <a href="{{route('posts.index')}}" class="btn btn-outline-info"> {{__('home.index.posts')}}</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
