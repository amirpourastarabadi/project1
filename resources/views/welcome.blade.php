@extends('layouts.main')


@section('page_title', __('home.index.welcome'))


@section('content')

    <div class="container">
        <div class="row" style="margin-top: 15%">
            <div class="col-12 text-center">
                <h1>CMS Project</h1>
            </div>
        </div>
    </div>



{{--    --}}{{--    header--}}
{{--    <div class="container">--}}
{{--        <div class="row my-4">--}}
{{--            <h4>{{__('posts.index.content.page_title')}}</h4>--}}
{{--        </div>--}}
{{--        @if(session('status'))--}}
{{--            <div class="row">--}}
{{--                <div class="col-12 alert alert-success text-center">{{session('status')}}</div>--}}
{{--            </div>--}}
{{--        @endif--}}

{{--        --}}{{--    main content--}}
{{--        <div class="row">--}}
{{--            <table class="table">--}}
{{--                <thead>--}}
{{--                <tr class="d-flex text-center">--}}
{{--                    <th scope="col" class="col-1">{{__('posts.index.content.id')}}</th>--}}
{{--                    <th scope="col" class="col-2">{{__('posts.index.content.title')}}</th>--}}
{{--                    <th scope="col" class="col-2">{{__('posts.index.content.slug')}}</th>--}}
{{--                    <th scope="col" class="col-2">{{__('posts.index.content.author')}}</th>--}}
{{--                    <th scope="col" class="col-1">{{__('posts.index.content.categories')}}</th>--}}
{{--                    <th scope="col" class="col-1">{{__('posts.index.content.tags')}}</th>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--                <tbody>--}}
{{--                @foreach($posts as $post)--}}
{{--                    <tr class="d-flex text-center">--}}
{{--                        <td class="col-1">{{$post->id}}</td>--}}
{{--                        <td class="col-2">--}}
{{--                            <a href="{{route('posts.show', $post)}}" class="text-dark">--}}
{{--                                {{$post->title}}--}}
{{--                            </a>--}}
{{--                        </td>--}}
{{--                        <td class="col-2">{{$post->slug}}</td>--}}
{{--                        <td class="col-2">{{$post->author->name}}</td>--}}
{{--                        <td class="col-1">--}}
{{--                            @foreach($post->categories as $category)--}}
{{--                                <span class="badge badge-dark">{{$category->title}}</span>--}}
{{--                            @endforeach--}}
{{--                        </td>--}}
{{--                        <td class="col-1">--}}
{{--                            @foreach($post->tags as $tag)--}}
{{--                                <span class="badge badge-dark">{{$tag->title}}</span>--}}
{{--                            @endforeach--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                @endforeach--}}

{{--                </tbody>--}}
{{--            </table>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
