@extends('layouts.main')

@section('page_title', 'داشبورد')

@section('content')
        <div class="card mt-5">
            <div class="card-header">
                {{auth()->user()->name}}
                خوش آمدید
            </div>
            <div class="card-body">
                <a href="{{route('posts.index')}}" class="card-title">
                    <h4>{{__('posts.index.page_title')}}</h4>
                </a>
            </div>
        </div>
@endsection
