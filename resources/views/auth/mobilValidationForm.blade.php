@extends('layouts.main')

@section('content')

    <div class="container">

        @isset($message)
            <div class="alert alert-danger text-center">
                {{$message}}
            </div>
        @endisset

        <div class="row  d-flex justify-content-center">
            <div class="col-6 mt-5">
                <div class="card mt-5">
                    <div class="card-header text-center">
                        {{__('auth.input_validation_code')}}
                    </div>
                    <div class="card-body">
                        <form action="{{route('verificationMobil')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="verification_code">{{__('auth.verification_code')}}</label>
                                <input type="text" class="form-control" id="verification_code" name="verification_code">
                                @error('verification_code') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="row  d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary my-3">{{__('auth.check_mobil')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
