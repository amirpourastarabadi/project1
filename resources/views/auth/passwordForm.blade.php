@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row  d-flex justify-content-center">
            <div class="col-6 mt-5">
                    <div class="card mt-5">
                        <div class="card-header text-center">
                            {{__('auth.input_password')}}
                        </div>
                        <div class="card-body">
                            <form action="{{route('login')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="password">{{__('auth.password')}}</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                    @error('password') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                                <div class="row  d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary my-3">{{__('auth.login')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>

@endsection
