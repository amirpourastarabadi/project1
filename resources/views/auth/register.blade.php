@extends('layouts.main')

@section('content')

    <div class="container">

        <div class="row  d-flex justify-content-center">
            <div class="col-6 mt-5">
                <div class="card mt-5">
                    <div class="card-header text-center">
                        {{__('auth.register.register')}}
                    </div>
                    <div class="card-body">
                        <form action="{{route('checkMobile')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="mobil">{{__('auth.mobil')}}</label>
                                <input type="text" class="form-control" id="mobil" name="mobil" value="{{$mobil}}" disabled>
                                @error('mobil') <span class="text-danger">{{$message}}</span> @enderror
                            </div>

                            @component('components.form-input', ['label'=>__('auth.register.name'),'type'=>'text','name'=>'name'])
                            @endcomponent


                            @component('components.form-input', ['label'=>__('auth.register.email'),'type'=>'email','name'=>'email'])
                            @endcomponent

                            @component('components.form-input', ['label'=>__('auth.register.password'),'type'=>'password','name'=>'password'])
                            @endcomponent

                            @component('components.form-input', ['label'=>__('auth.register.password_confirmation'),'type'=>'password','name'=>'password'])
                            @endcomponent

                            <div class="row  d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary my-3">{{__('auth.register.submit')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
