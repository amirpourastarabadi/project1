<?php

return [

    'index' => [
        'page_title' => "لیست پست ها",
        'content' => [
            'status' => 'پست جدید با موفقیت اضافه شد',
            'delete_alert' => 'پست با موفقیت حذف شد',
            'page_title' => 'نمایش تمامی پست ها',
            'id' => "آیدی",
            'content' => 'محتوا',
            "title" => "عنوان پست",
            "slug" => "Slug",
            "author" => "نام نویسنده",
            "categories" => "دسته بندی ها",
            "tags" => "تگ ها",
            "operations" => "عملیات",
            "delete" => 'حذف',
            'show' => 'مشاهده',
            'create_post' => 'ایجاد پست جدید'
        ]
    ],
    'create' => [
        'page_title' => 'ایجاد پست جدید',
        'content' => [
            'page_title' => 'ساخت پست جدید',
            'form' => [
                'form_title' => [
                    'categories' => 'دسته بندی ها',
                    'title' => 'عنوان پست',
                    'content' => 'محتوا',
                    'slug' => 'slug',
                    'image' => 'تصویر',
                    'alt' => 'متن جایگزین عکس',
                    'tags' => 'برچسب'
                ],
                'submit' => 'ذخیره'
            ]
        ]
    ],
    'show' => [
        'no_categories' => 'این پست در گروه خاصی قرار ندارد',
        'no_tags' => 'این پست بدون نشانه است',
        'title' => 'مشخصات کامل پست',
    ]


];
