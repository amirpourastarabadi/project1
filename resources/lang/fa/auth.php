<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'رمز عبور',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'ورود',
    'check_mobil' => 'بررسی شماره همراه',
    'mobil' => 'تلفن همراه',
    'input_mobil' => 'شماره همراه خود را وارد کنید',
    'input_password' => 'رمز عبور خود را وارد کنید',
    'invalid_password' => 'اطلاعات حساب کاربری صحیح نمی باشد. مجدد تلاش کنید',
    'input_validation_code' => 'کد ارسال شده به شماره همراه خود را در کادر زیر وارد کنید',
    'verification_code' => 'کد تایید',
    'register' => [
        'register' => 'ورود اطلاعات کاربری',
        'name'=>'نام',
        'email'=>'ایمیل',
        'password'=>'رمز عبور',
        'password_confirmation'=>'تکرار رمز عبور',
        'submit'=>'عضویت',

    ],
    'messages' => [
        'bye' => 'خدانگهدار',
        'invalidValidationCode' => 'کد وارد شده صحیح نمی باشد'
    ],

];
