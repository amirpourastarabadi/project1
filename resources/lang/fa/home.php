<?php

return [
    'index' => [
        'posts' => 'پست های شما',
        'dashboard' => 'داشبورد',
        'welcome' => 'خوش آمدید',
        'home' => 'خانه',
        'login' => 'ورود',
        'register' => 'ثبت نام',
        'logout' => 'خروج'
    ]
];
