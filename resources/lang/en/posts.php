<?php

return [

    'index' => [
        'content' => [
            'page_title' => 'All Posts',
            'id' => "ID",
            "title" => "Title",
            "slug" => "Slug",
            "author" => "Author",
            "categories" => "Categories",
            "tags" => "Tags",
            "operations" => "Operations",
            "delete" => 'Delete',
            'show' => 'Show'
        ]
    ],


];
